<?php require 'components/layout/head.php'; ?>
<?php require 'components/layout/header.php'; ?>
<?php require 'app/utils/database.php'; ?>

<?php
$errors=[];
if( isset($_POST['registrace']) ) {
    $_POST['registerEmail']=trim(@$_POST['registerEmail']);
    if ($_POST['registerEmail']!='' && !filter_var($_POST['registerEmail'],FILTER_VALIDATE_EMAIL)){
        $errors[]='Zadaný e-mail není platný!';
    }else {
        $email = htmlspecialchars($_POST['registerEmail']);
        $stmt = $db->prepare("INSERT INTO newsletter(email) VALUES (?)");
        $stmt->execute(array($email));
        echo ('<div class="alert alert-success" role="alert">
     <div class="container">Email úspěšně zaregistrován k newsletteru</div>
    </div>');
    }
}
?>

<div class="container mb-5">
    <h2>Registrace do newsletteru</h2>
    <?php
    if (!empty($errors)){
        echo '<ul style="color:red;">';
        foreach ($errors as $error){
            echo '<li>'.$error.'</li>';
        }
        echo '</ul>';
    }
    ?>
    <form method="post">
        <div class="input-group mb-3">
            <input type="hidden" name="registrace" value="registrace">
            <input name="registerEmail" class="form-control" type="email"
                   placeholder="Email pro registraci newsletteru" required>
        </div>
        <input type="submit" value="Registrovat email" class="btn btn-primary">
    </form>
</div>
<?php require'components/layout/footer.php'; ?>

