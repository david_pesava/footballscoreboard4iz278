<?php require 'components/layout/head.php'; ?>
<?php require 'components/layout/header.php'; ?>

<?php
session_start();
require 'app/utils/database.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $email = $_POST['email'];
    $password = $_POST['password'];

    $stmt = $db->prepare("SELECT * FROM users WHERE email = ? LIMIT 1"); //limit 1 jen jako vykonnostni optimalizace, 2 stejne maily se v db nepotkaji
    $stmt->execute(array($email));
    $existing_user = @$stmt->fetchAll()[0];

    if (password_verify($password, $existing_user["password"])) {
        $_SESSION['user_id'] = $existing_user["id"];
        header('Location: admin/admin-index.php');
    } else {
        die("Invalid user or password!");
    }
}
?>

<div class="container">
    <h2>Přihlášení do administrace</h2>
    <div class="row">
        <div class="col-12 col-md-4 offset-md-3">

            <div class="form-group">
                <form action="" method="POST">
                    Email<br/>
                    <input class="form-control" type="text" name="email" value=""><br/><br/>
                    Heslo<br/>
                    <input class="form-control" type="password" name="password" value=""><br/><br/>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>


<?php require 'components/layout/footer.php'; ?>
