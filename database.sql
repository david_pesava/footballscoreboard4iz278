-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Počítač: localhost
-- Vytvořeno: Pon 03. čen 2019, 09:53
-- Verze serveru: 10.2.17-MariaDB-log
-- Verze PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `pesd02`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `fbUsers`
--

CREATE TABLE `fbUsers` (
  `id` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL,
  `favTeam` varchar(50) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `fbUsers`
--

INSERT INTO `fbUsers` (`id`, `favTeam`) VALUES
('2479877212030822', '1');

-- --------------------------------------------------------

--
-- Struktura tabulky `matches`
--

CREATE TABLE `matches` (
  `id` int(11) NOT NULL,
  `homeTeam` int(3) NOT NULL,
  `awayTeam` int(3) NOT NULL,
  `date` date NOT NULL,
  `pointsHome` int(3) NOT NULL,
  `pointsAway` int(3) NOT NULL,
  `winner` int(5) NOT NULL,
  `round` int(2) NOT NULL,
  `yards` varchar(10) NOT NULL,
  `penalties` varchar(10) NOT NULL,
  `touchdowns` varchar(10) NOT NULL,
  `fieldgoalds` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `homeLineup` text NOT NULL,
  `awayLineup` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `matches`
--

INSERT INTO `matches` (`id`, `homeTeam`, `awayTeam`, `date`, `pointsHome`, `pointsAway`, `winner`, `round`, `yards`, `penalties`, `touchdowns`, `fieldgoalds`, `description`, `homeLineup`, `awayLineup`) VALUES
(1, 1, 2, '2019-05-27', 43, 21, 1, 1, '120-50', '0-2', '4-1', '6-1', 'Zápas 1. ligy AF v ČR, první kolo', '4,5,6', '7,8,9'),
(16, 2, 1, '2019-06-04', 68, 64, 2, 2, '130-108', '1-14', '2-5', '5-1', 'Zápas 1. ligy AF v ČR, první kolo', '7,8,9', '4,5,6');

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(4) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`) VALUES
(1, 'as@test.cz'),
(2, 'as@test.cz'),
(3, 'as@test.cz');

-- --------------------------------------------------------

--
-- Struktura tabulky `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `fullName` varchar(30) NOT NULL,
  `number` varchar(2) NOT NULL,
  `type` varchar(15) NOT NULL,
  `position` varchar(24) NOT NULL,
  `team` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `players`
--

INSERT INTO `players` (`id`, `fullName`, `number`, `type`, `position`, `team`) VALUES
(4, 'James Novák', '99', 'Defense', 'Safety', 1),
(5, 'Tom Brady', '12', 'Offense', 'Quarterback', 1),
(6, 'John Test', '23', 'Special team', 'Kicker', 1),
(7, 'Joe Test', '55', 'Defense', 'Tight end', 2),
(8, 'Terry Test', '98', 'Offense', 'Center', 2),
(9, 'Tom Test', '1', 'Special team\"', 'Kicker', 2);

-- --------------------------------------------------------

--
-- Struktura tabulky `teams`
--

CREATE TABLE `teams` (
  `id` int(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `stadium` varchar(30) NOT NULL,
  `coach` varchar(15) NOT NULL,
  `percentage` float NOT NULL,
  `pointsScored` int(6) NOT NULL,
  `pointsReceived` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `teams`
--

INSERT INTO `teams` (`id`, `name`, `city`, `stadium`, `coach`, `percentage`, `pointsScored`, `pointsReceived`) VALUES
(1, 'Prague Lions', 'Praha', 'Hostivař', 'coach', 0.5, 107, 89),
(2, 'Vysočina Gladiators', 'Jihlava', 'Jihlava', 'coach', 0.5, 89, 107),
(3, 'Pardubice Stallions', 'Pardubice', 'Pardubice', 'coach', 0, 0, 0),
(4, 'Pilsen Patriots', 'Plzeň', 'Plzeň', 'coach', 0, 0, 0),
(5, 'Brno Aligators', 'Brno', 'Brno', 'coach', 0, 0, 0),
(6, 'Ústí nad Labem Blades', 'Ústí nad Labem', 'Ústí nad Labem', 'coach', 0, 0, 0),
(7, 'Brno Sigrs', 'Brno', 'Brno', 'coach', 0, 0, 0),
(8, 'Ostrava Steelers', 'Ostrava', 'Ostrava', 'coach', 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'admin@admin.cz', '$2y$10$2bSAt8LDEUPPK6XfNXH/cOw8RyKqZIzZXKq.kjVpZu9OODL348I8i');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team` (`team`);

--
-- Klíče pro tabulku `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `matches`
--
ALTER TABLE `matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pro tabulku `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pro tabulku `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pro tabulku `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
