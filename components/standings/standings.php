<?php
require 'app/controllers/standings/standings.php';
?>
<div class="card mb-5">
    <div class="card-header"><strong>Tabulka soutěže</strong></div>
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Tým</th>
                    <th scope="col">Points</th>
                    <th scope="col">%</th>
                </tr>
                </thead>
                <?php foreach($teams as $team) { ?>
                    <tr>
                        <td><a href="page-team-detail.php?team=<?php echo($team['id']); ?>" class="col-3 text-center mb-2"><?= $team['name'] ?> </a></td>
                        <td><?= $team['pointsScored'] ?> - <?= $team['pointsReceived'] ?></td>
                        <td><?= number_format((float)$team['percentage'], 3, '.', '') ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
</div>

