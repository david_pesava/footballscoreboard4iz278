<?php
require 'app/controllers/team-detail/team-detail.php';
?>

<div>
    <div class="row">
    <?php foreach($team as $teamInfo) { ?>
        <div class="col-2"> <img src="assets/img/teams/<?php echo($teamInfo['id']); ?>.png" alt="<?php echo($teamInfo['id']); ?>"></div>
        <div class="col-10 d-flex align-items-center">
        <h2><?= $teamInfo['name'] ?> </h2> -
        <span> <?= $teamInfo['city'] ?> </span> -
        <span> <?= $teamInfo['stadium'] ?> </span>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">%</th>
                <th scope="col">Scored</th>
                <th scope="col">Recevied</th>
                <th scope="col">Point conversion</th>
            </tr>
            </thead>
            <tr>
                <td><?= $teamInfo['percentage'] ?></td>
                <td><?= $teamInfo['pointsScored'] ?></td>
                <td><?= $teamInfo['pointsReceived'] ?></td>
                <td><?= number_format((float)$teamInfo['pointsScored']/$teamInfo['pointsReceived'], 2, '.', '') ?></td>
            </tr>
        </table>
    <?php } ?>
    </div>
    <div class="mb-5">
        <h3>Hráči</h3>
        <?php foreach($players as $player) {
            if($player['type'] === 'offense') ?>
                <strong><?= $player['fullName'] ?></strong> - <span><?= $player['position'] ?></span> -  #<span><?= $player['number'] ?></span><br>
        <?php } ?>
    </div>
    <div class="mb-5">
<!--        <h3>Hráči - defense</h3>-->
        <?php foreach($players as $player) {
            if($player['type'] === 'offense') ?>
                <strong><?= $player['fullName'] ?></strong> - <span><?= $player['position'] ?></span> -  #<span><?= $player['number'] ?></span>
        <?php } ?>
    </div>
    <div class="mb-5">
<!--        <h3>Hráči - special teams</h3>-->
        <?php foreach($players as $player) {
            if($player['type'] === 'st') ?>
                <strong><?= $player['fullName'] ?></strong> - <span><?= $player['position'] ?></span> -  #<span><?= $player['number'] ?></span>
        <?php } ?>
    </div>
    <div class="mb-5">
        <h3>Domácí zápasy</h3>
        <?php foreach ($teamsLastMatchesHome as $match) { ?>
            <a href="page-match-detail.php?match=<?php echo($match['id']); ?>">
                <div class="mb-3">
                    <div class="text-center">
                        <?php $matchTime = new DateTime($match['date']); ?>
                        <h5 class="d-inline"><?= date_format($matchTime, "d.m.Y") ?></h5> -
                        <h5 class="d-inline"><?= $match['round'] ?>. kolo ligy</h5>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="d-flex align-items-center text-center justify-content-center">
                                <img src="assets/img/teams/<?php echo($match['homeTeam']); ?>.png" alt="<?php echo($match['homeTeam']); ?>"
                                     class="latestMatches__teamLogo mr-1">
                                <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['homeTeam'] . ' ') as $homeTeamName) { ?>
                                    <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $homeTeamName['name'] ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-2 text-center d-flex align-items-center justify-content-center">
                            <strong><?= $match['pointsHome'] ?></strong>
                            <strong>:</strong>
                            <strong><?= $match['pointsAway'] ?></strong>
                        </div>
                        <div class="col-5">
                            <div class="d-flex align-items-center text-center justify-content-center">
                                <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['awayTeam'] . ' ') as $awayTeamName) { ?>
                                    <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $awayTeamName['name'] ?></div>
                                <?php } ?>
                                <img src="assets/img/teams/<?php echo($match['awayTeam']); ?>.png" alt="<?php echo($match['awayTeam']); ?>"
                                     class="latestMatches__teamLogo ml-1">
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <hr>
        <?php } ?>
    </div>
    <div class="mb-5">
        <h3>Venkovní zápasy</h3>
        <?php foreach ($teamsLastMatchesAway as $match) { ?>
            <a href="page-match-detail.php?match=<?php echo($match['id']); ?>">
                <div class="mb-3">
                    <div class="text-center">
                        <?php $matchTime = new DateTime($match['date']); ?>
                        <h5 class="d-inline"><?= date_format($matchTime, "d.m.Y") ?></h5> -
                        <h5 class="d-inline"><?= $match['round'] ?>. kolo ligy</h5>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="d-flex align-items-center text-center justify-content-center">
                                <img src="assets/img/teams/<?php echo($match['homeTeam']); ?>.png" alt="<?php echo($match['homeTeam']); ?>"
                                     class="latestMatches__teamLogo mr-1">
                                <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['homeTeam'] . ' ') as $homeTeamName) { ?>
                                    <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $homeTeamName['name'] ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-2 text-center d-flex align-items-center justify-content-center">
                            <strong><?= $match['pointsHome'] ?></strong>
                            <strong>:</strong>
                            <strong><?= $match['pointsAway'] ?></strong>
                        </div>
                        <div class="col-5">
                            <div class="d-flex align-items-center text-center justify-content-center">
                                <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['awayTeam'] . ' ') as $awayTeamName) { ?>
                                    <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $awayTeamName['name'] ?></div>
                                <?php } ?>
                                <img src="assets/img/teams/<?php echo($match['awayTeam']); ?>.png" alt="<?php echo($match['awayTeam']); ?>"
                                     class="latestMatches__teamLogo ml-1">
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            <hr>
        <?php } ?>
    </div>
</div>