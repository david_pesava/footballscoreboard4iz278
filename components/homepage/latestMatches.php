<?php
require 'app/controllers/homepage/latestMatches.php';
?>

<div class="card mb-5">
    <div class="card-header"><strong>Zápasy aktuálního kola</strong></div>
    <div class="card-body">
        <?php foreach ($latestMatches as $match) { ?>
        <a href="page-match-detail.php?match=<?php echo($match['id']); ?>">
        <div class="mb-3">
            <?php $matchTime = new DateTime($match['date']); ?>
            <h5 class="d-inline"><?= date_format($matchTime, "d.m.Y") ?></h5> -
            <h5 class="d-inline"><?= $match['round'] ?>. kolo ligy</h5>
            <div class="row">
                <div class="col-5">
                    <div class="d-flex align-items-center text-center">
                        <img src="assets/img/teams/<?php echo($match['homeTeam']); ?>.png" alt="<?php echo($match['homeTeam']); ?>"
                             class="latestMatches__teamLogo mr-1">
                        <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['homeTeam'] . ' ') as $homeTeamName) { ?>
                            <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $homeTeamName['name'] ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-2 text-center d-flex align-items-center">
                    <strong><?= $match['pointsHome'] ?></strong>
                    <strong>:</strong>
                    <strong><?= $match['pointsAway'] ?></strong>
                </div>
                <div class="col-5">
                    <div class="d-flex align-items-center text-center">
                        <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['awayTeam'] . ' ') as $awayTeamName) { ?>
                            <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $awayTeamName['name'] ?></div>
                        <?php } ?>
                        <img src="assets/img/teams/<?php echo($match['awayTeam']); ?>.png" alt="<?php echo($match['awayTeam']); ?>"
                             class="latestMatches__teamLogo ml-1">
                    </div>
                </div>
            </div>
        </div>
        </a>
            <hr>
        <?php } ?>
    </div>
</div>