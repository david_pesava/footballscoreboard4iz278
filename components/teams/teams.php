<?php
require 'app/controllers/teams/teams.php';
?>

<div class="row mb-5">
    <?php foreach($teams as $team) { ?>
        <a href="page-team-detail.php?team=<?php echo($team['id']); ?>" class="col-3 text-center mb-2">
            <img src="assets/img/teams/<?php echo($team['id']); ?>.png" class="mb-2" alt="<?php echo($team['id']); ?>">
            <h4><?= $team['name'] ?></h4>
        </a>
    <?php } ?>
</div>
