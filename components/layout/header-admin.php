<nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-5">
    <div class="container">
        <strong>FootAdmin</strong>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="admin-index.php">Úvodní stránka</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin-teams.php">Týmy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin-matches.php">Zápasy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin-players.php">Hráči</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin-newsletter.php">Odeslání newsletteru</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php"><strong>Odhlásit se</strong></a>
                </li>
            </ul>
        </div>
    </div>

</nav>