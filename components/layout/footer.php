<footer class="page-footer font-small bg-light pt-4">
    <div class="container">
        <div class="text-center text-md-left">
            <div class="row">
                <div class="col-md-6 mt-md-0 mt-3">
                    <h5 class="text-uppercase">FootballScoreBoard</h5>
                    <p>Výsledky z české ligy amerického fotbalu.</p>
                </div>
                <hr class="clearfix w-100 d-md-none pb-3">
                <div class="col-md-3"></div>
                <div class="col-md-3 mb-md-0 mb-3">
                    <ul class="list-unstyled">
                        <li>
                            <a href="page-teams.php">Týmy</a>
                        </li>
                        <li>
                            <a href="page-players.php">Hráči</a>
                        </li>
                        <li>
                            <a href="page-matches.php">Zápasy</a>
                        </li>
                        <li>
                            <a href="page-newsletter.php">Newsletter</a>
                        </li>
                        <li>
                            <a href="admin-login.php">Admin</a>
                        </li>
                        <li>
                            <a href="https://gitlab.com/david_pesava/footballscoreboard4iz278" target="_blank">Zdrojový
                                kod webu</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer-copyright text-center py-3">© 2019 Copyright:
                <a href="https://gitlab.com/david_pesava/footballscoreboard4iz278" target="_blank">David Pešava pro
                    4iz278</a>
            </div>
        </div>
</footer>
</body>
</html>
