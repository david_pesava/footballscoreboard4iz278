<nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-5">
    <div class="container">
        <h1>
        <a class="navbar-brand" href="index.php">
            <img src="assets/img/logo.png" alt="Football Score Board" class="navbar__logo">
        </a>
            <span class="d-none">Football Score Board</span>
        </h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Úvodní stránka</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="page-teams.php">Týmy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="page-matches.php">Zápasy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="page-players.php">Hráči</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="page-newsletter.php">Newsletter</a>
                </li>
            </ul>
        </div>
    </div>
</nav>