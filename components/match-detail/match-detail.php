<?php
require 'app/controllers/match-detail/match-detail.php';
?>

<?php foreach ($matches as $match) { ?>
        <div class="mb-3">
            <div class="text-center">
                <h2>Zápas</h2>
                <?php $matchTime = new DateTime($match['date']); ?>
                <h3 class="d-inline"><?= date_format($matchTime, "d.m.Y") ?></h3> -
                <h3 class="d-inline"><?= $match['round'] ?>. kolo ligy</h3>
            </div>
            <div class="row">
                <div class="col-5">
                    <div class="d-flex align-items-center text-center justify-content-center">
                        <img src="assets/img/teams/<?php echo($match['homeTeam']); ?>.png" alt="<?php echo($match['homeTeam']); ?>"
                             class="latestMatches__teamLogo mr-1">
                        <?php foreach ($db->query('SELECT name,id FROM teams WHERE id=' . $match['homeTeam'] . ' ') as $homeTeamName) { ?>
                            <div class="latestMatches__teamName text-center d-flex align-items-center">
                                <a href="page-team-detail.php?team=<?php echo($homeTeamName['id']); ?>"><?= $homeTeamName['name'] ?> </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-2 text-center d-flex align-items-center justify-content-center">
                    <strong><?= $match['pointsHome'] ?></strong>
                    <strong>:</strong>
                    <strong><?= $match['pointsAway'] ?></strong>
                </div>
                <div class="col-5">
                    <div class="d-flex align-items-center text-center justify-content-center">
                        <?php foreach ($db->query('SELECT name,id FROM teams WHERE id=' . $match['awayTeam'] . ' ') as $awayTeamName) { ?>
                            <div class="latestMatches__teamName text-center d-flex align-items-center">
                                 <a href="page-team-detail.php?team=<?php echo($awayTeamName['id']); ?>"><?= $awayTeamName['name'] ?> </a>
                            </div>
                        <?php } ?>
                        <img src="assets/img/teams/<?php echo($match['awayTeam']); ?>.png" alt="<?php echo($match['awayTeam']); ?>"
                             class="latestMatches__teamLogo ml-1">
                    </div>
                </div>
            </div>
        </div>
    <div class="mb-3 text-center">
        <p><?= $match['description'] ?></p>
    </div>
    <div class="mb-5 text-center">
        <h4>Statistiky zápasu</h4>
            <hr>
            <strong>Yardy celkem</strong>
            <div><?= $match['yards'] ?></div>
            <hr>
            <strong>Penalizace (yardy)</strong>
            <div><?= $match['penalties'] ?></div>
            <hr>
            <strong>Touchdowns</strong>
            <div><?= $match['touchdowns'] ?></div>
            <hr>
            <strong>Fieldsgoals</strong>
            <div><?= $match['fieldgoalds'] ?></div>
    </div>
    <h4 class="text-center">Sestavy</h4>
    <div class="row mb-5">
        <div class="col-12 col-lg-6 text-left">
            <?php
            $homeToCut = $match['homeLineup'];
            $searchHome = array("array", "(", ")", "'");
            $parsedHome = explode(',',str_replace($searchHome, '', $homeToCut));
            foreach ($parsedHome as $key => $val) {
                foreach ($db->query('SELECT fullName,number,position,type FROM players WHERE id=' . $val . ' ') as $playerHome) {
                    echo ($playerHome['fullName'] . ' ');
                    echo ('- #'.$playerHome['number'] . ' - ');
                    echo ($playerHome['type'] . ' - ');
                    echo ($playerHome['position'] . ' ');
                    echo ('<br>');
                }
            }
            ?>
        </div>
        <div class="col-12 col-lg-6 text-right">
            <?php
            $awayToCut = $match['awayLineup'];
            $searchAway = array("array", "(", ")", "'");
            $parsedAway = explode(',',str_replace($searchAway, '', $awayToCut));
            foreach ($parsedAway as $key => $val) {
                foreach ($db->query('SELECT fullName,number,position,type FROM players WHERE id=' . $val . ' ') as $playerAway) {
                    echo ($playerAway['fullName'] . ' ');
                    echo ('- #'.$playerAway['number'] . ' - ');
                    echo ($playerAway['type'] . ' - ');
                    echo ($playerAway['position'] . ' ');
                    echo ('<br>');
                }
            }
            ?>
        </div>
    </div>
<?php } ?>