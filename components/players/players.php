<?php
require 'app/controllers/players/players.php';
?>

<div>
    <h2 class="mb-3">Aktivní hráči v lize</h2>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Jméno</th>
            <th scope="col">Pozice</th>
            <th scope="col">Formace</th>
            <th scope="col">Číslo</th>
            <th scope="col">Tým</th>
        </tr>
        </thead>
        <?php foreach($players as $player) { ?>
            <tr>
                <td><strong><?= $player['fullName'] ?></strong></td>
                <td><?= $player['position'] ?></td>
                <td><?= $player['type'] ?></td>
                <td><?= $player['number'] ?></td>
                <td>
                    <?php foreach ($db->query('SELECT name,id FROM teams WHERE id=' . $player['team'] . ' ') as $team) { ?>
                    <strong><a href="page-team-detail.php?team=<?php echo($team['id']); ?>"><?= $team['name'] ?><?php } ?></a></strong>
                    <?php } ?>
                </td>
            </tr>
    </table>
</div>