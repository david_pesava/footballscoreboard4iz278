<?php require 'components/layout/head.php'; ?>
<?php require 'components/layout/header.php'; ?>
<div class="container">
    <h2 class="mb-3">Zápasy</h2>
    <div class="row">
        <div class="col-12">
            <?php require 'components/matches/matches.php'; ?>
        </div>
    </div>
</div>
<?php require'components/layout/footer.php'; ?>
