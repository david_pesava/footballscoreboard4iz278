<?php require 'components/layout/head.php'; ?>
<?php require 'components/layout/header.php'; ?>
<?php require 'app/utils/database.php'; ?>
<?php
session_start();
$loggedUser = false;

ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
require_once __DIR__ . '/sdkfb/src/Facebook/autoload.php';
$fb = new Facebook\Facebook([
    'app_id' => '2487479588147465',
    'app_secret' => '456c6ef1e482fc11c88864a164f46674',
    'default_graph_version' => 'v2.2',
]);
$helper = $fb->getRedirectLoginHelper();
try {
    $accessToken = $helper->getAccessToken('https://eso.vse.cz/~pesd02/fb-callback.php');
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
if (!isset($accessToken)) {
    if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
    }
    exit;
}

// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();
// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);
$response = $fb->get('/me?fields=name,link', $accessToken);
//var_dump($tokenMetadata["user_id"]);
// Validation (these will throw FacebookSDKException's when they fail)
$tokenMetadata->validateAppId('2487479588147465'); // TODO Replace {app-id} with your app-id
// If you know the user ID this access token belongs to, you can validate it here
//$tokenMetadata->validateUserId('123');
$tokenMetadata->validateExpiration();
if (!$accessToken->isLongLived()) {
    // Exchanges a short-lived access token for a long-lived one
    try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
        exit;
    }
}
$_SESSION['fb_access_token'] = (string)$accessToken;
$user = $response->getGraphUser();
if (isset($_POST['homeTeam'])) {
    $homeTeam = htmlspecialchars($_POST['homeTeam']);
    $stmtHome = $db->prepare("UPDATE fbUsers SET favTeam=? WHERE id=?");
    $stmtHome->execute(array($homeTeam, $user['id']));
}

$loggedUser = true;

?>
<?php if ($loggedUser) { ?>

    <?php
    $stmt = $db->prepare('SELECT * FROM `fbUsers` WHERE id=' . $user['id'] . ' ');
    $stmt->execute();
    $myLoggedUser = $stmt->fetchAll();
    echo('<div class="container">
    <h2>Přihlášen jako ' . $user["name"] . '</h2><div><strong>Zápasy vašeho oblíbeného týmu:</strong></div>');
    if (empty($myLoggedUser)) {
        $stmt = $db->prepare("INSERT INTO fbUsers(id,favTeam) VALUES (?,?)");
        $stmt->execute(array($user['id'], '1'));
    } else {

        foreach ($myLoggedUser as $user) {
            $stmtA = $db->prepare('SELECT * FROM `matches` WHERE id=' . $user['favTeam'] . ' ');
            $stmtA->execute();
            $favTeamMatches = $stmtA->fetchAll();

            ?>
            <?php foreach ($favTeamMatches as $match) { ?>
                <a href="page-match-detail.php?match=<?php echo($match['id']); ?>">
                    <div class="mb-3">
                        <?php $matchTime = new DateTime($match['date']); ?>
                        <h5 class="d-inline"><?= date_format($matchTime, "d.m.Y") ?></h5> -
                        <h5 class="d-inline"><?= $match['round'] ?>. kolo ligy</h5>
                        <div class="row">
                            <div class="col-5">
                                <div class="d-flex align-items-center text-center">
                                    <img src="assets/img/teams/<?php echo($match['homeTeam']); ?>.png" alt="home team"
                                         class="latestMatches__teamLogo mr-1">
                                    <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['homeTeam'] . ' ') as $homeTeamName) { ?>
                                        <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $homeTeamName['name'] ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-2 text-center d-flex align-items-center">
                                <strong><?= $match['pointsHome'] ?></strong>
                                <strong>:</strong>
                                <strong><?= $match['pointsAway'] ?></strong>
                            </div>
                            <div class="col-5">
                                <div class="d-flex align-items-center text-center">
                                    <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['awayTeam'] . ' ') as $awayTeamName) { ?>
                                        <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $awayTeamName['name'] ?></div>
                                    <?php } ?>
                                    <img src="assets/img/teams/<?php echo($match['awayTeam']); ?>.png" alt="away team"
                                         class="latestMatches__teamLogo ml-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <hr>
            <?php }
        }
    }
    ?>
    <form action="fb-callback.php" method="post">
        <div class="form-group">
            <label for="homeTeam">Změnit oblíbený tým</label>
            <select class="form-control" name="homeTeam" required>
                <option value="1">Prague Lions</option>
                <option value="2">Vysočina Gladiators</option>
                <option value="3">Pardubice Stallions</option>
                <option value="4">Pilsen Patriots</option>
                <option value="5">Brno Aligators</option>
                <option value="6">Ústí nad Labem Blades</option>
                <option value="7">Brno Sigrs</option>
                <option value="8">Ostrava Steelers</option>
            </select>
        </div>
        <input class="btn btn-primary mb-5" type="submit" value="Odeslat">
    </form>
<?php } ?>
</div>
<?php require 'components/layout/footer.php'; ?>

