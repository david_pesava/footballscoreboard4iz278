<?php require 'components/layout/head.php'; ?>
<?php require 'components/layout/header.php'; ?>
<?php
session_start();
ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
require_once 'sdkfb/src/Facebook/autoload.php';
$fb = new Facebook\Facebook([
    'app_id' => '2487479588147465', // Replace {app-id} with your app id
    'app_secret' => '456c6ef1e482fc11c88864a164f46674',
    'default_graph_version' => 'v3.2',
]);
$helper = $fb->getRedirectLoginHelper();
$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://eso.vse.cz/~pesd02/fb-callback.php', $permissions);
?>


<div class="container">
    <div class="row">
        <div class="col-12 col-md-6">
            <?php require 'components/standings/standings.php'; ?>
        </div>
        <div class="col-12 col-md-6">
            <?php require 'components/homepage/latestMatches.php'; ?>
        </div>
    </div>

    <?php echo '<a href="' . htmlspecialchars($loginUrl) . '">Uživatelský profil(sledování oblíbeného týmu)</a>'; ?>
</div>
<?php require 'components/layout/footer.php'; ?>
