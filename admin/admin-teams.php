<?php require '../components/layout/head.php'; ?>
<?php require '../components/layout/header-admin.php'; ?>
<?php require 'utils/database.php'; ?>
<?php require 'utils/user.php'; ?>

<?php
$isEditing = false;
$errors = [];

$stmt = $db->prepare('SELECT * FROM `teams` ORDER BY `teams`.`name` ASC');
$stmt->execute();
$teams = $stmt->fetchAll();

if(isset($_GET['teamIdUpdate'])) {
    $teamIdUpdate = $_GET['teamIdUpdate'];
    $isEditing = true;
    $teamToEdit = $db->query('SELECT * FROM `teams` WHERE id='.$teamIdUpdate.' ');
}
if( isset($_POST['teamIdE']) ) {


    $_POST['teamNameE']=trim(@$_POST['teamNameE']);
    if (!preg_match("/^[a-zA-Z -.]{5,}$/",$_POST['teamNameE'])){
        $errors[]='Text musí mít alespoň 5 znaků!.';
    }

    $_POST['teamStadiumE']=trim(@$_POST['teamStadiumE']);
    if (!preg_match("/^[a-zA-Z -.]{5,}$/",$_POST['teamStadiumE'])){
        $errors[]='Text musí mít alespoň 3 znaků!.';
    }

    $_POST['teamCityE']=trim(@$_POST['teamCityE']);
    if (!preg_match("/^[a-zA-Z -.]{5,}$/",$_POST['teamCityE'])){
        $errors[]='Text musí mít alespoň 3 znaků!.';
    }

    $_POST['teamCoachE']=trim(@$_POST['teamCoachE']);
    if (!preg_match("/^[a-zA-Z -.]{5,}$/",$_POST['teamCoachE'])){
        $errors[]='Text musí mít alespoň 3 znaků!.';
    }

    $teamIdE = htmlspecialchars($_POST['teamIdE']);
    $teamNameE = htmlspecialchars($_POST['teamNameE']);
    $teamStadiumE = htmlspecialchars($_POST['teamStadiumE']);
    $teamCityE = htmlspecialchars($_POST['teamCityE']);
    $teamCoachE = htmlspecialchars($_POST['teamCoachE']);

    if (empty($errors)) {
        $stmt = $db->prepare("UPDATE teams SET name=?, stadium=?, city=?, coach=? WHERE id=?");
        $stmt->execute(array($teamNameE,$teamStadiumE,$teamCityE,$teamCoachE,$teamIdE));
        header('Location: admin-teams.php');
    }
}
?>
<div class="container">
    <?php
    if (!empty($errors)){
        echo '<ul style="color:red;">';
        foreach ($errors as $error){
            echo '<li>'.$error.'</li>';
        }
        echo '</ul>';
    }
    ?>
    <?php if($isEditing) {?>
        <?php foreach($teamToEdit as $team) { ?>
            Upravujete tým:
            <strong><?= $team['name'] ?></strong> z <?= $team['city'] ?>
            <form action="admin-teams.php" method="post">
                <input type="hidden" name="teamIdE" value="<?= $team['id'] ?>">
                <div class="form-group">
                    <label class="control-label" for="teamNameE">Název týmu</label>
                    <input class="form-control" type="text" name="teamNameE" value="<?= $team['name'] ?>" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="teamStadiumE">Stadion</label>
                    <input class="form-control" type="text" name="teamStadiumE" value="<?= $team['stadium'] ?>" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="teamCityE">Město</label>
                    <input class="form-control" type="text" name="teamCityE" value="<?= $team['city'] ?>" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="teamCoachE">Coach</label>
                    <input class="form-control" type="text" name="teamCoachE" value="<?= $team['coach'] ?>" required>
                </div>
                <input class="btn btn-primary mb-5" type="submit" value="Upravit">
            </form>
        <?php } ?>
    <?php } ?>
    <hr>
    <h2 class="mb-3">Týmy</h2>
    <table class="table mb-5">
        <thead>
        <tr>
            <th scope="col">Název</th>
            <th scope="col">Město</th>
            <th scope="col">Stadion</th>
            <th scope="col">Hlavní trenér</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <?php foreach($teams as $team) { ?>
            <tr>
                <td><strong><?= $team['name'] ?></strong></td>
                <td><?= $team['city'] ?></td>
                <td><?= $team['stadium'] ?></td>
                <td><?= $team['coach'] ?></td>
                <td>
                    <a href="admin-teams.php?teamIdUpdate=<?php echo($team['id']); ?>">Editovat</a>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>