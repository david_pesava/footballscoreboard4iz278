<?php require 'utils/database.php'; ?>
<?php require 'utils/user.php'; ?>
<?php require '../components/layout/head.php'; ?>
<?php require '../components/layout/header-admin.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-header">Týmy</div>
                <div class="card-body">Možnost změny údajů jednotlivých týmů ligy
                    <div class="my-4"><a href="admin-teams.php" class="btn btn-primary">Více</a></div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Zápasy</div>
                <div class="card-body">Přidání/odebrání/editace zápasů ligy
                    <div class="my-4"><a href="admin-matches.php" class="btn btn-primary">Více</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Hráči</div>
                <div class="card-body">Přidání/odebrání/editace hrůčů
                    <div class="my-4"><a href="admin-players.php" class="btn btn-primary">Více</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">Newsletter</div>
                <div class="card-body">Správa newsletteru
                    <div class="my-4"><a href="admin-newsletter.php" class="btn btn-primary">Více</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
