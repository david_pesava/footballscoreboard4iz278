<?php require '../components/layout/head.php'; ?>
<?php require '../components/layout/header-admin.php'; ?>
<?php require 'utils/database.php'; ?>
<?php require 'utils/user.php'; ?>

<?php
$errors=[];
$isEditing = false;
$stmt = $db->prepare('SELECT * FROM `players` ORDER BY team');
$stmt->execute();
$players = $stmt->fetchAll();

if( isset($_POST['playerName']) ) {

    if (!in_array($_POST['playerPosition'],['Offensive guard','Offensive tackl','Quarterback','Running back',
        'Wide receive','Tight end','Defensive tackle','Defensive end','Middle linebacker','Outside linebacker','Cornerback',
        'Safety','Kicker','Holder','Punter'])){
        $errors[]='Pozice špatně vybrána!';
    }

    if (!in_array($_POST['playerFormation'],['Offense','Defense','Special team'])){
        $errors[]='Formace špatně vybrána!';
    }

    if (($_POST['playerNumber'] > 99) || ($_POST['playerNumber'] < 1)) {
        $errors[]='Moc velké číslo hráče!';
    }

    $_POST['playerName']=trim(@$_POST['playerName']);
    if (!preg_match("/^[a-zA-Z -.]{5,}$/",$_POST['playerName'])){
        $errors[]='Jméno a příjmení musí mít alespoň 5 znaků!.';
    }

    if (!in_array($_POST['playerTeam'],[1,2,3,4,5,6,7,8])){
        $errors[]='Tým špatně zadán!!';
    }

    $playerName = htmlspecialchars($_POST['playerName']);
    $playerPosition = htmlspecialchars($_POST['playerPosition']);
    $playerFormation = htmlspecialchars($_POST['playerFormation']);
    $playerNumber = htmlspecialchars($_POST['playerNumber']);
    $playerTeam = htmlspecialchars($_POST['playerTeam']);

    if (empty($errors)) {
    $stmt = $db->prepare("INSERT INTO players(fullName,number,type,position,team) VALUES (?,?,?,?,?)");
    $stmt->execute(array($playerName, $playerNumber, $playerFormation, $playerPosition, $playerTeam));
    header('Location: admin-players.php');
    }
}
if(isset($_GET['playerId'])) {
    $playerId = $_GET['playerId'];
    $stmt = $db->prepare("DELETE FROM players WHERE id=?");
    $stmt->execute(array($playerId));
    header('Location: admin-players.php');
}
if(isset($_GET['playerIdUpdate'])) {
    $playerIdUpdate = $_GET['playerIdUpdate'];
    $isEditing = true;
    $playerToEdit = $db->query('SELECT * FROM `players` WHERE id='.$playerIdUpdate.' ');
}
if( isset($_POST['playerNameE']) ) {

    if (!in_array($_POST['playerPositionE'],['Offensive guard','Offensive tackl','Quarterback','Running back',
        'Wide receive','Tight end','Defensive tackle','Defensive end','Middle linebacker','Outside linebacker','Cornerback',
        'Safety','Kicker','Holder','Punter'])){
        $errors[]='Pozice špatně vybrána!';
    }

    if (!in_array($_POST['playerFormationE'],['Offense','Defense','Special team'])){
        $errors[]='Formace špatně vybrána!';
    }

    if (($_POST['playerNumberE'] > 99) || ($_POST['playerNumberE'] < 1)) {
        $errors[]='Moc velké číslo hráče!';
    }

    $_POST['playerNameE']=trim(@$_POST['playerNameE']);
    if (!preg_match("/^[a-zA-Z -.]{5,}$/",$_POST['playerNameE'])){
        $errors[]='Jméno a příjmení musí mít alespoň 5 znaků!.';
    }

    if (!in_array($_POST['playerTeamE'],[1,2,3,4,5,6,7,8])){
        $errors[]='Tým špatně zadán!!';
    }

    $stmt = $db->prepare("SELECT last_updated_at FROM players WHERE id = ?");
    $stmt->execute(array($_POST['playerIdE']));
    $current_last_updated_at = $stmt->fetchColumn();

//    var_dump($_POST['last_updated_at'] != $current_last_updated_at);
    if ($_POST['last_updated_at'] === $current_last_updated_at) {
        die ("Hráče momentálně nelze upravit, už to dělá někdo jiný ,zkuste to prosím později :)");
    }

    $playerIdE = htmlspecialchars($_POST['playerIdE']);
    $playerNameE = htmlspecialchars($_POST['playerNameE']);
    $playerPositionE = htmlspecialchars($_POST['playerPositionE']);
    $playerFormationE = htmlspecialchars($_POST['playerFormationE']);
    $playerNumberE = htmlspecialchars($_POST['playerNumberE']);
    $playerTeamE = htmlspecialchars($_POST['playerTeamE']);

    if (empty($errors)) {
        $stmt = $db->prepare("UPDATE players SET fullName=?, number=?, position=?, type=?, team=?,last_updated_at=now() WHERE id=?");
        $stmt->execute(array($playerNameE,$playerNumberE,$playerFormationE,$playerPositionE,$playerTeamE,$playerIdE));
        header('Location: admin-players.php');
    }
}
?>

<div class="container">
    <h2 class="mb-3">Aktivní hráči v lize</h2>
    <table class="table mb-5">
        <thead>
        <tr>
            <th scope="col">Osobní číslo</th>
            <th scope="col">Jméno</th>
            <th scope="col">Pozice</th>
            <th scope="col">Formace</th>
            <th scope="col">Číslo</th>
            <th scope="col">Tým</th>
            <th scope="col">Akce</th>
        </tr>
        </thead>
        <?php foreach($players as $player) { ?>
        <tr>
            <td><strong><?= $player['id'] ?></strong></td>
            <td><strong><?= $player['fullName'] ?></strong></td>
            <td><?= $player['position'] ?></td>
            <td><?= $player['type'] ?></td>
            <td><?= $player['number'] ?></td>
            <td>
                <?php foreach ($db->query('SELECT name,id FROM teams WHERE id=' . $player['team'] . ' ') as $team) { ?>
                <strong><?= $team['name'] ?><?php } ?></strong>

            </td>
            <td>
                <a href="admin-players.php?playerId=<?php echo($player['id']); ?>">Smazat</a> /
                <a href="admin-players.php?playerIdUpdate=<?php echo($player['id']); ?>">Editovat</a>
            </td>
        </tr>
        <?php } ?>
    </table>
    <?php
    if (!empty($errors)){
        echo '<ul style="color:red;">';
        foreach ($errors as $error){
            echo '<li>'.$error.'</li>';
        }
        echo '</ul>';
    }
    ?>
    <h2 class="<?php if($isEditing){echo('d-none');} ?>" id="#addPlayer">Přidání hráče</h2>
    <form action="admin-players.php" method="post" class="<?php if($isEditing){echo('d-none');} ?>">
        <div class="form-group">
            <label class="control-label" for="playerName">Celé jméno hráče</label>
            <input class="form-control" type="text" name="playerName" required>
        </div>
        <div class="form-group">
            <label for="playerPosition">Pozice</label>
            <select class="form-control" name="playerPosition" required>
                <option value="Center">Center</option>
                <option value="Offensive guard">Offensive guard</option>
                <option value="Offensive tackl">Offensive tackle</option>
                <option value="Quarterback">Quarterback</option>
                <option value="Running back">Running back</option>
                <option value="Wide receive<">Wide receive</option>
                <option value="Tight end">Tight end</option>
                <option value="Defensive tackle">Defensive tackle</option>
                <option value="Defensive end">Defensive end</option>
                <option value="Middle linebacker">Middle linebacker</option>
                <option value="Outside linebacker">Outside linebacker</option>
                <option value="Cornerback">Cornerback</option>
                <option value="Safety">Safety</option>
                <option value="Kicker">Kicker</option>
                <option value="Holder">Holder</option>
                <option value="Punter">Punter</option>
            </select>
        </div>
        <div class="form-group">
            <label for="playerFormation">Formace</label>
            <select class="form-control" name="playerFormation" required>
                <option value="Offense">Offense</option>
                <option value="Defense">Defense</option>
                <option value="Special team">Special team</option>
            </select>
        </div>
        <div class="form-group">
            <label for="playerTeam">Tým</label>
            <select class="form-control" name="playerTeam" required>
                <option value="1">Prague Lions</option>
                <option value="2">Vysočina Gladiators</option>
                <option value="3">Pardubice Stallions</option>
                <option value="4">Pilsen Patriots</option>
                <option value="5">Brno Aligators</option>
                <option value="6">Ústí nad Labem Blades</option>
                <option value="7">Brno Sigrs</option>
                <option value="8">Ostrava Steelers</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" for="playerNumber">Číslo</label>
            <input class="form-control" type="number" name="playerNumber" min="1" max="99" placeholder="Vyberte číslo (1-99)" required>
        </div>
        <input class="btn btn-primary mb-5" type="submit" value="Odeslat">
    </form>
    <?php if($isEditing) {?>
        <div class="mb-4"><a href="admin-players.php">Přidat hráče</a></div>
        <hr>
        <?php foreach($playerToEdit as $player) { ?>
            Upravujete hráče:
            <strong><?= $player['fullName'] ?></strong> s číslem #<?= $player['number'] ?>
    <form action="admin-players.php" method="post">
        <input type="hidden" name="playerIdE" value="<?= $player['id'] ?>">
        <input type="hidden" name="last_updated_at" value="<?= $player['last_updated_at	'] ?>">
        <div class="form-group">
            <label class="control-label" for="playerNameE">Celé jméno hráče</label>
            <input class="form-control" type="text" name="playerNameE" value="<?= $player['fullName'] ?>" required>
        </div>
        <div class="form-group">
            <label for="playerPositionE">Pozice</label>
            <select class="form-control" name="playerPositionE" required>
                <option value="" selected disabled hidden>Vyberte pozici</option>
                <option value="Center">Center</option>
                <option value="Offensive guard">Offensive guard</option>
                <option value="Offensive tackl">Offensive tackle</option>
                <option value="Quarterback">Quarterback</option>
                <option value="Running back">Running back</option>
                <option value="Wide receive<">Wide receive</option>
                <option value="Tight end">Tight end</option>
                <option value="Defensive tackle">Defensive tackle</option>
                <option value="Defensive end">Defensive end</option>
                <option value="Middle linebacker">Middle linebacker</option>
                <option value="Outside linebacker">Outside linebacker</option>
                <option value="Cornerback">Cornerback</option>
                <option value="Safety">Safety</option>
                <option value="Kicker">Kicker</option>
                <option value="Holder">Holder</option>
                <option value="Punter">Punter</option>
            </select>
        </div>
        <div class="form-group">
            <label for="playerFormationE">Formace</label>
            <select class="form-control" name="playerFormationE" required>
                <option value="" selected disabled hidden>Vyberte formaci</option>
                <option value="Offense">Offense</option>
                <option value="Defense">Defense</option>
                <option value="Special team">Special team</option>
            </select>
        </div>
        <div class="form-group">
            <label for="playerTeamE">Tým</label>
            <select class="form-control" name="playerTeamE" required>
                <option value="" selected disabled hidden>Vyberte tým</option>
                <option value="1">Prague Lions</option>
                <option value="2">Vysočina Gladiators</option>
                <option value="3">Pardubice Stallions</option>
                <option value="4">Pilsen Patriots</option>
                <option value="5">Brno Aligators</option>
                <option value="6">Ústí nad Labem Blades</option>
                <option value="7">Brno Sigrs</option>
                <option value="8"></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" for="playerNumberE">Číslo</label>
            <input class="form-control" type="number" name="playerNumberE" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
        </div>
        <input class="btn btn-primary mb-5" type="submit" value="Odeslat">
    </form>
        <?php } ?>
    <?php } ?>
</div>