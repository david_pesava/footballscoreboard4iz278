<?php require '../components/layout/head.php'; ?>
<?php require '../components/layout/header-admin.php'; ?>
<?php require 'utils/database.php'; ?>
<?php require 'utils/user.php'; ?>

<?php

if(isset($_POST['odeslani'])){
    $zprava = '<h1>Dobrý den,</h1><h2>zasíláme vám přehled novinek z FootBallScoreBoard:</h2><br><br>';
    $textFromForm = htmlspecialchars($_POST['message']);
    $zprava .= $textFromForm;
    $prijemci = '';
    foreach($db->query('SELECT * FROM `newsletter` WHERE 1') as $row) {
        $prijemci .= $row['email'].',';
    }
    $predmet  = 'Football Newsletter';
    $hlavicky = [
        'MIME-Version: 1.0',
        'Content-type: text/html; charset=utf-8',
        'From: pesd02@vse.cz',
        'Reply-To: pesd02@vse.cz',
        'X-Mailer: PHP/'.phpversion()
    ];
    $hlavicky = implode("\r\n", $hlavicky);
    if (mail($prijemci, $predmet, $zprava, $hlavicky)){
        echo '<div class="alert alert-success text-center" role="alert">
        Newsletter odeslán!
      </div>';
    }else{
        echo '<div class="alert alert-danger text-center" role="alert">
        Email se nepodařilo odeslat!
      </div>';
    }
}
?>
<div class="container">
    <h2>Odeslání hromadné zprávy uživatelům registrovaným k newsletteru</h2>
    <form method="post" class="mb-5">
        <div class="form-group">
        <input type="hidden" name="odeslani" value="odeslani">
        <textarea required rows="9" class="form-control" name="message"></textarea>
        </div>
        <input type="submit" value="Odeslat" class="btn btn-primary">
    </form>
    <div class="card mb-3">
        <div class="card-header"><h2>Registrované emaily:</h2></div>
        <div class="card-body">
            <div class="mb-3">
                <?php foreach($db->query('SELECT * FROM `newsletter` WHERE 1') as $row) { ?>
                    <div>
                        <div><?= $row['email'] ?></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>