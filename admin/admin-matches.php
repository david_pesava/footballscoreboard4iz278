<?php require '../components/layout/head.php'; ?>
<?php require '../components/layout/header-admin.php'; ?>
<?php require 'utils/database.php'; ?>
<?php require 'utils/user.php'; ?>

<?php
$errors=[];
$stmt = $db->prepare('SELECT * FROM `matches`');
$stmt->execute();
$matches = $stmt->fetchAll();

$isEditing = false;

if( isset($_POST['newMatch']) ) {

    if($_POST['homeTeam'] === $_POST['awayTeam']) {
        $errors[]='Tým nemůže hrát sám se sebou.';
    }

    if (($_POST['yHome'] > 99) || ($_POST['yHome'] < 1)) {
        $errors[]='YD domácí - číslo mimo rozsah (1-99)';
    }

    if (($_POST['scoreHome'] > 99) || ($_POST['scoreHome'] < 1)) {
        $errors[]='Skore domácí - číslo mimo rozsah (1-99)';
    }

    if (($_POST['penaltyHome'] > 99) || ($_POST['penaltyHome'] < 1)) {
        $errors[]='Tresty domácí - číslo mimo rozsah (1-99)';
    }

    if (($_POST['touchdownHome'] > 99) || ($_POST['touchdownHome'] < 1)) {
        $errors[]='TD domácí - číslo mimo rozsah (1-99)';
    }

    if (($_POST['fieldgoalsHome'] > 99) || ($_POST['fieldgoalsHome'] < 1)) {
        $errors[]='FD domácí - číslo mimo rozsah (1-99)';
    }
    $_POST['lineupHome']=trim(@$_POST['lineupHome']);
    if (!preg_match("/[0-99]/",$_POST['lineupHome'])){
        $errors[]='Zadávejte ve tvaru: číslo hráče,číslo hráče....';
    }

    if (($_POST['yAway'] > 99) || ($_POST['yAway'] < 1)) {
        $errors[]='YD hosté - číslo mimo rozsah (1-99)';
    }

    if (($_POST['scoreAway'] > 99) || ($_POST['scoreAway'] < 1)) {
        $errors[]='Skore hosté - číslo mimo rozsah (1-99)';
    }

    if (($_POST['penaltyAway'] > 99) || ($_POST['penaltyAway'] < 1)) {
        $errors[]='Tresty hosté - číslo mimo rozsah (1-99)';
    }

    if (($_POST['touchdownAway'] > 99) || ($_POST['touchdownAway'] < 1)) {
        $errors[]='TD hosté - číslo mimo rozsah (1-99)';
    }

    if (($_POST['fieldgoalsAway'] > 99) || ($_POST['fieldgoalsAway'] < 1)) {
        $errors[]='FD hosté - číslo mimo rozsah (1-99)';
    }
    $_POST['lineupAway']=trim(@$_POST['lineupAway']);
    if (!preg_match("/[0-99]/",$_POST['lineupAway'])){
        $errors[]='Zadávejte ve tvaru: číslo hráče,číslo hráče....';
    }

    $matchDate = htmlspecialchars($_POST['matchDate']);
    $matchDesc = htmlspecialchars($_POST['matchDesc']);
    $round = htmlspecialchars($_POST['round']);

    $homeTeam = htmlspecialchars($_POST['homeTeam']);
    $yHome = htmlspecialchars($_POST['yHome']);
    $scoreHome = htmlspecialchars($_POST['scoreHome']);
    $penaltyHome = htmlspecialchars($_POST['penaltyHome']);
    $touchdownHome = htmlspecialchars($_POST['touchdownHome']);
    $fieldgoalsHome = htmlspecialchars($_POST['fieldgoalsHome']);

    $lineupHome = htmlspecialchars($_POST['lineupHome']);

    $awayTeam = htmlspecialchars($_POST['awayTeam']);
    $yAway = htmlspecialchars($_POST['yAway']);
    $scoreAway = htmlspecialchars($_POST['scoreAway']);
    $penaltyAway = htmlspecialchars($_POST['penaltyAway']);
    $touchdownAway = htmlspecialchars($_POST['touchdownAway']);
    $fieldgoalsAway = htmlspecialchars($_POST['fieldgoalsAway']);

    $lineupAway = htmlspecialchars($_POST['lineupAway']);

    $points = $scoreHome.'-'.$scoreAway;
    $penalties = $penaltyHome.'-'.$penaltyAway;
    $touchdowns = $touchdownHome.'-'.$touchdownAway;
    $fieldgoals= $fieldgoalsHome.'-'.$fieldgoalsAway;
    $yards= $yHome.'-'.$yAway;

    if (empty($errors)) {

    $winner = 0;
    if ($scoreHome > $scoreAway) {
        $winner = $homeTeam;
    } else {
        $winner = $awayTeam;
    }

    $stmt = $db->prepare("INSERT INTO matches(homeTeam,awayTeam,date,pointsHome,pointsAway,round,penalties,touchdowns,fieldgoalds,description,homeLineup,awayLineup,winner,yards) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $stmt->execute(array($homeTeam, $awayTeam, $matchDate, $scoreHome, $scoreAway, $round, $penalties, $touchdowns, $fieldgoals, $matchDesc, $lineupHome, $lineupAway, $winner, $yards));

    $aHomeTeam = $db->query('SELECT * FROM teams WHERE id=' . $homeTeam . ' ');
    $aAwayTeam = $db->query('SELECT * FROM teams WHERE id=' . $awayTeam . ' ');

    $askHomeMatchCountTotal = $db->prepare('SELECT COUNT(id) FROM matches WHERE (awayTeam = '.$homeTeam.')or(awayTeam =' .$homeTeam.')');
    $askHomeMatchCountTotal->execute();
    $aHomeMatchCountTotal = ($askHomeMatchCountTotal->fetchAll());

    $askHomeWins = $db->prepare('SELECT COUNT(id) FROM matches WHERE winner = ' . $homeTeam . ' ');
    $askHomeWins->execute();
    $aHomeWins = ($askHomeWins->fetchAll());

    $askAwayMatchCountTotal = $db->prepare('SELECT COUNT(id) FROM matches WHERE (awayTeam = '.$awayTeam.')or(awayTeam =' .$awayTeam.')');
    $askAwayMatchCountTotal->execute();
    $aAwayMatchCountTotal = ($askAwayMatchCountTotal->fetchAll());

    $askAwayWins = $db->prepare('SELECT COUNT(id) FROM matches WHERE winner = ' . $awayTeam . ' ');
    $askAwayWins->execute();
    $aAwayWins = ($askAwayWins->fetchAll());

    if ((int)$aHomeMatchCountTotal[0]["COUNT(id)"] <= 0 || (int)$aHomeWins[0]["COUNT(id)"] <= 0) {
        $newHomePercentage = 0;
    } else {
        $newHomePercentage = (int)$aHomeWins[0]["COUNT(id)"] / (int)$aHomeMatchCountTotal[0]["COUNT(id)"];
    }
    if ((int)$aAwayMatchCountTotal[0]["COUNT(id)"] <= 0 || (int)$aAwayWins[0]["COUNT(id)"] <= 0) {
        $newAwayPercentage = 0;
    } else {
        $newAwayPercentage = (int)$aAwayWins[0]["COUNT(id)"] /(int)$aAwayMatchCountTotal[0]["COUNT(id)"];
    }

    $homeScore = 0;
    $homeRec = 0;
    $awayScore = 0;
    $awayRec = 0;

    foreach ($aHomeTeam as $team) {
        $homeScore = $team["pointsScored"];
        $homeRec = $team["pointsReceived"];
        $homeScoreNew = (int)$homeScore + (int)$scoreHome;
        $homeRecNew = (int)$homeRec + (int)$scoreAway;
        $stmtHome = $db->prepare("UPDATE teams SET pointsScored=?,pointsReceived=?, percentage=?  WHERE id=?");
        $stmtHome->execute(array($homeScoreNew, $homeRecNew, $newHomePercentage, $homeTeam));
    }

    foreach ($aAwayTeam as $team) {
        $awayScore = $team["pointsScored"];
        $awayRec = $team["pointsReceived"];
        $awayScoreNew = (int)$awayScore + (int)$scoreAway;
        $awayRecNew = (int)$awayRec + (int)$scoreHome;
        $stmtAway = $db->prepare("UPDATE teams SET pointsScored=?,pointsReceived=?, percentage=?  WHERE id=?");
        $stmtAway->execute(array($awayScoreNew, $awayRecNew, $newAwayPercentage, $awayTeam));
    }
    header('Location: admin-matches.php');
    }
}

if(isset($_GET['deleteMatchId'])) {
    $deleteMatchId = $_GET['deleteMatchId'];
    $stmt = $db->prepare("DELETE FROM matches WHERE id=?");
    $stmt->execute(array($deleteMatchId));
    header('Location: admin-matches.php');
}
?>

<div class="container">
    <?php if(!$isEditing) {?>
    <h2>Přidání zápasu</h2>
        <?php
        if (!empty($errors)){
            echo '<ul style="color:red;">';
            foreach ($errors as $error){
                echo '<li>'.$error.'</li>';
            }
            echo '</ul>';
        }
        ?>
    <form action="admin-matches.php" method="post">
        <input type="hidden" value="newMatch" name="newMatch">
        <div class="form-group">
            <label class="control-label" for="matchDate">Datum zápasu</label>
            <input class="form-control" type="Date" name="matchDate" required>
        </div>

        <div class="form-group">
            <label class="control-label" for="matchDesc">Popis zápasu</label>
            <input class="form-control" type="text" name="matchDesc" required>
        </div>

        <div class="form-group">
            <label class="control-label" for="round">Číslo ligového kola nebo zápasu</label>
            <input class="form-control" type="text" name="round" required>
        </div>

        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="homeTeam">Domácí tým</label>
                    <select class="form-control" name="homeTeam" required>
                        <option value="1">Prague Lions</option>
                        <option value="2">Vysočina Gladiators</option>
                        <option value="3">Pardubice Stallions</option>
                        <option value="4">Pilsen Patriots</option>
                        <option value="5">Brno Aligators</option>
                        <option value="6">Ústí nad Labem Blades</option>
                        <option value="7">Brno Sigrs</option>
                        <option value="8">Ostrava Steelers</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="scoreHome">Points - domácí</label>
                    <input class="form-control" type="number" name="scoreHome" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="yHome">Yards - domácí</label>
                    <input class="form-control" type="number" name="yHome" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="penaltyHome">Penalizace - domácí</label>
                    <input class="form-control" type="number" name="penaltyHome" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="touchdownHome">Touchdowny- domácí</label>
                    <input class="form-control" type="number" name="touchdownHome" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="fieldgoalsHome">Fieldgoals- domácí</label>
                    <input class="form-control" type="number" name="fieldgoalsHome" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="lineupHome">Sestava - domácí - zadejte osobní číslo hráčů oddělené čárkou</label>
                    <input class="form-control" type="text" name="lineupHome" required placeholder="např: 1,2,3">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="awayTeam">Hostující tým</label>
                    <select class="form-control" name="awayTeam" required>
                        <option value="1">Prague Lions</option>
                        <option value="2">Vysočina Gladiators</option>
                        <option value="3">Pardubice Stallions</option>
                        <option value="4">Pilsen Patriots</option>
                        <option value="5">Brno Aligators</option>
                        <option value="6">Ústí nad Labem Blades</option>
                        <option value="7">Brno Sigrs</option>
                        <option value="8">Ostrava Steelers</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="scoreAway">Points - hosté</label>
                    <input class="form-control" type="number" name="scoreAway" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="yAway">Yards - hosté</label>
                    <input class="form-control" type="number" name="yAway" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="penaltyAway">Penalizace - hosté</label>
                    <input class="form-control" type="number" name="penaltyAway" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="touchdownAway">Touchdowny - hosté</label>
                    <input class="form-control" type="number" name="touchdownAway" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="fieldgoalsAway">Touchdowny- hosté</label>
                    <input class="form-control" type="number" name="fieldgoalsAway" required min="1" max="99" placeholder="Vyberte číslo (1-99)">
                </div>
                <div class="form-group">
                    <label class="control-label" for="lineupAway">Sestava - hosté - zadejte osobní číslo hráčů oddělené čárkou</label>
                    <input class="form-control" type="text" name="lineupAway" required placeholder="např: 1,2,3">
                </div>
            </div>
        </div>
        <input class="btn btn-primary mb-5" type="submit" value="Přidat">
    </form>
    <?php } ?>

<?php foreach ($matches as $match) { ?>
        <div class="mb-3">
            <div class="text-center">
                <?php $matchTime = new DateTime($match['date']); ?>
                <h5 class="d-inline"><?= date_format($matchTime, "d.m.Y") ?></h5> -
                <h5 class="d-inline"><?= $match['round'] ?>. kolo ligy</h5>
            </div>
            <div class="row">
                <div class="col-5">
                    <div class="d-flex align-items-center text-center justify-content-center">
                        <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['homeTeam'] . ' ') as $homeTeamName) { ?>
                            <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $homeTeamName['name'] ?></div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-2 text-center d-flex align-items-center justify-content-center">
                    <strong><?= $match['pointsHome'] ?></strong>
                    <strong>:</strong>
                    <strong><?= $match['pointsAway'] ?></strong>
                </div>
                <div class="col-5">
                    <div class="d-flex align-items-center text-center justify-content-center">
                        <?php foreach ($db->query('SELECT name FROM teams WHERE id=' . $match['awayTeam'] . ' ') as $awayTeamName) { ?>
                            <div class="latestMatches__teamName text-center d-flex align-items-center"><?= $awayTeamName['name'] ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="text-center"><a href="admin-matches.php?deleteMatchId=<?php echo($match['id']) ?>">Smazat</a></div>
        </div>
    <hr>
<?php } ?>
</div>