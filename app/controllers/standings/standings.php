<?php
require 'app/utils/database.php';

$stmt = $db->prepare('SELECT * FROM `teams` ORDER BY `teams`.`percentage` DESC');
$stmt->execute();
$teams = $stmt->fetchAll();