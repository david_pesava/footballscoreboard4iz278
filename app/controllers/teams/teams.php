<?php
require 'app/utils/database.php';

$stmt = $db->prepare('SELECT * FROM `teams` ORDER BY `teams`.`name` ASC');
$stmt->execute();
$teams = $stmt->fetchAll();