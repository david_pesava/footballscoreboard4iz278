<?php
session_start();
if(!isset($_SESSION["user_id"])){
    header('Location: index.php');
    die();
}
$stmt = $db->prepare("SELECT * FROM users WHERE id = ? LIMIT 1"); //limit 1 jen jako vykonnostni optimalizace, 2 stejne maily se v db nepotkaji
$stmt->execute(array($_SESSION["user_id"]));
$current_user = $stmt->fetchAll()[0]; //vezmi prvni zaznam z db
if (!$current_user){
    session_destroy();
    header('Location: index.php');
    die();
}
?>
