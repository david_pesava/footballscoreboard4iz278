<?php require 'components/layout/head.php'; ?>
<?php require 'components/layout/header.php'; ?>
<div class="container">
    <h2 class="mb-3">Týmy soutěže ligy</h2>
    <?php require 'components/teams/teams.php'; ?>
    <div class="row">
        <div class="col-12">
            <?php require 'components/standings/standings.php'; ?>
        </div>
    </div>
</div>
<?php require'components/layout/footer.php'; ?>
